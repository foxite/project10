using UnityEngine;

namespace Warehouse {
	public class AnimateBelt : MonoBehaviour {
		[SerializeField] private int m_MaterialIndex = 0;
		[SerializeField] private Direction m_Direction;
		[SerializeField] private float m_Speed = 0.1f;

		private Renderer m_Renderer;
		private float m_XOffset = 0;

		private void Awake() {
			m_Renderer = GetComponent<Renderer>();
		}

		private void Update() {
			if (m_Renderer.enabled) {
				m_XOffset += m_Speed * Time.deltaTime * (m_Direction == Direction.Forward ? -1f : 1f);
				m_Renderer.materials[m_MaterialIndex].mainTextureOffset = new Vector2(m_XOffset, m_Direction == Direction.Forward ? 0 : 0.5f);
			}
		}

		public enum Direction {
			Forward, Backward
		}
	}
}
