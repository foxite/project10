﻿using UnityEngine;
using UnityEngine.Events;

namespace Warehouse {
	public class TestButton : MonoBehaviour {
		[SerializeField] private bool m_Press = false;
		[SerializeField] private UnityEvent m_OnPress;

		private void Update() {
			if (m_Press) {
				m_Press = false;
				m_OnPress.Invoke();
			}
		}
	}
}
