﻿using UnityEngine;

namespace Warehouse {
	public class BoxSpawnPoint : MonoBehaviour {
		public bool Occupied { get; set; }

		private static bool s_Quitting = false;

		private void OnEnable() {
			BoxSpawner.Instance.AddPoint(this);
		}

		private void OnApplicationQuit() {
			s_Quitting = true;
		}

		private void OnDisable() {
			if (!s_Quitting) {
				BoxSpawner.Instance.RemovePoint(this);
			}
		}
	}
}
