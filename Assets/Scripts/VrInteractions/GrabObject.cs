﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(SteamVR_Behaviour_Pose))]
public class GrabObject : MonoBehaviour
{
    public SteamVR_Action_Boolean gripInput;
    public SteamVR_Action_Boolean triggerInput;

    public SteamVR_Behaviour_Pose trackedObject;
    public ConfigurableJoint configJoint;

    private InteractableObject currentObject;

    private GameObject currentlyInteractingWith;


    private bool isCurrentlyGrabbingAnOnject;

    // Start is called before the first frame update
    void Start()
    {
        trackedObject = trackedObject ?? GetComponent<SteamVR_Behaviour_Pose>();
        configJoint = configJoint ?? GetComponent<ConfigurableJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gripInput.GetStateDown(trackedObject.inputSource))
        {
            GrabCurrentObject();
        }
        if (gripInput.GetStateUp(trackedObject.inputSource))
        {
            DropCurrentObject();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Identify Object 
        if(other.gameObject.TryGetComponent(out currentObject))
        {
            currentlyInteractingWith = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //remove interactable abilities with the object
        if(other == currentlyInteractingWith)
        {
            currentlyInteractingWith = null;
            currentObject = null;
        }
    }

    void GrabCurrentObject()
    {
        if(currentObject.canBePickedUp == true && isCurrentlyGrabbingAnOnject == false)
        {
            isCurrentlyGrabbingAnOnject = true;
            Rigidbody rig;
            if(currentlyInteractingWith.TryGetComponent(out rig))
            {
                rig.useGravity = false;
            }
            configJoint.connectedBody = rig;
        }
    }

    void DropCurrentObject()
    {
        if(isCurrentlyGrabbingAnOnject == true)
        {
            Rigidbody rig;
            if(currentlyInteractingWith.TryGetComponent(out rig))
            {
                rig.useGravity = true;
            }
            configJoint.connectedBody = null;

            isCurrentlyGrabbingAnOnject = false;
        }
    }
}
