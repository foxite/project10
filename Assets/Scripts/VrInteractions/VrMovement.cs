﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public enum PlayerMovementType
{
    Teleport,
    Trackpad
}
public class VrMovement : MonoBehaviour
{
    public PlayerMovementType movementType;

    public SteamVR_Action_Boolean triggerInput;

    public SteamVR_Behaviour_Pose trackedObject;

    public GameObject vrRig;
    /* Raycast values */
    Ray ray;
    RaycastHit rayHit;

    // Start is called before the first frame update
    void Start()
    {
        if (movementType == PlayerMovementType.Trackpad)
        {
            throw new System.NotImplementedException();   
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (triggerInput.GetStateDown(trackedObject.inputSource))
        {
            if(Physics.Raycast(trackedObject.transform.position, trackedObject.transform.forward, out rayHit))
            {
                vrRig.transform.position = rayHit.transform.position;
            }
        }
    }
}
