﻿using UnityEngine;

namespace Warehouse {
	[CreateAssetMenu]
	public class BoxDefinition : ScriptableObject {
		[SerializeField] private Box m_Prefab;
		[SerializeField] private float m_Size;

		public float Size => m_Size;

		public Box CreateInstance(ItemDefinition itemDef) {
			Box instance = Instantiate(m_Prefab);
			instance.AssignDefinitionAndItem(this, itemDef);
			return instance;
		}
	}
}
