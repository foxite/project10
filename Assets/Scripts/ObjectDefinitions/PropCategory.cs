﻿using System.Collections.Generic;
using UnityEngine;

namespace Warehouse {
	public class PropCategory : ScriptableObject {
		[SerializeField] private Prop[] m_Props;

		public IReadOnlyList<Prop> Props => m_Props;

		public Prop GetRandomProp() {
			return m_Props[Random.Range(0, m_Props.Length)];
		}
	}
}