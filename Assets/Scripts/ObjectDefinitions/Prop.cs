﻿using UnityEngine;

namespace Warehouse {
	public class Prop : ScriptableObject {
		[SerializeField] private GameObject m_Prefab;

		public Transform SpawnOnHotspot(PropHotspot hotspot) {
			Transform instanceTransform = Instantiate(m_Prefab, hotspot.transform, true).transform;
			instanceTransform.transform.SetPositionAndRotation(m_Prefab.transform.position, m_Prefab.transform.rotation);
			return instanceTransform;
		}
	}
}