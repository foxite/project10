﻿using UnityEngine;

namespace Warehouse {
	[CreateAssetMenu]
	public class ItemDefinition : ScriptableObject {
		[SerializeField] private string m_ItemName;
		[SerializeField] private Item m_Prefab;
		[SerializeField] private float m_Size;

		public string ItemName => m_ItemName;
		public float Size => m_Size;

		public Item CreateInstance(Box box) {
			Item item = Instantiate(m_Prefab);
			item.AssignDefinitionAndBox(this, box);
			return item;
		}
	}
}