﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR.Extras;

namespace Warehouse
{
    [RequireComponent(typeof(SteamVR_LaserPointer))]
    public class VrUiPointer : MonoBehaviour
    {
        private void Start()
        {
            SteamVR_LaserPointer laser = GetComponent<SteamVR_LaserPointer>();
            laser.PointerIn += Laser_PointerIn;
            laser.PointerOut += Laser_PointerOut;
            laser.PointerClick += Laser_PointerClick;
            Destroy(this);
        }

        private static void Laser_PointerClick(object sender, PointerEventArgs e)
        {
            if (e.target.TryGetComponent<IPointerClickHandler>(out var peh))
            {
                Debug.Log("HEY!");
                peh.OnPointerClick(new PointerEventData(EventSystem.current));
            }
        }

        private static void Laser_PointerOut(object sender, PointerEventArgs e)
        {
            if (e.target.TryGetComponent<IPointerExitHandler>(out var peh))
            {
                Debug.Log("HEY!");
                peh.OnPointerExit(new PointerEventData(EventSystem.current));
            }
        }

        private static void Laser_PointerIn(object sender, PointerEventArgs e)
        {
            if (e.target.TryGetComponent<IPointerEnterHandler>(out var peh))
            {
                Debug.Log("HEY!");
                peh.OnPointerEnter(new PointerEventData(EventSystem.current));
            }
        }
    }
}
