﻿using TMPro;
using UnityEngine;

namespace Warehouse {
	public class ScannerMenu : MonoBehaviour {
		[SerializeField] private TMP_Text m_CurrentTitle, m_LeftTitle, m_RightTitle;
		[SerializeField] private RectTransform[] m_Tabs;

		private int m_CurrentTabIndex = 0;

		private int GetLeftTabIndex() {
			if (m_CurrentTabIndex == 0) {
				return m_Tabs.Length - 1;
			} else {
				return m_CurrentTabIndex - 1;
			}
		}

		private int GetRightTabIndex() {
			if (m_CurrentTabIndex == m_Tabs.Length - 1) {
				return 0;
			} else {
				return m_CurrentTabIndex + 1;
			}
		}

		private void UpdateTabLabels() {
			m_CurrentTitle.text = m_Tabs[m_CurrentTabIndex ].name;
			m_LeftTitle   .text = m_Tabs[GetLeftTabIndex() ].name;
			m_RightTitle  .text = m_Tabs[GetRightTabIndex()].name;
		}

		/// <summary>
		/// Call this with -1 to move to the right tab, or 1 to switch to the left.
		/// </summary>
		/// <param name="direction"></param>
		private void SwitchTabs(int direction) {
			m_Tabs[m_CurrentTabIndex].gameObject.SetActive(false);
			if (direction < 0) {
				m_CurrentTabIndex = GetLeftTabIndex();
			} else {
				m_CurrentTabIndex = GetRightTabIndex();
			}
			m_Tabs[m_CurrentTabIndex].gameObject.SetActive(true);
			UpdateTabLabels();
		}
		
		// These are used by the Buttons
		public void GoLeft () => SwitchTabs(-1);
		public void GoRight() => SwitchTabs (1);

		private void Update() {
			if (Input.GetKeyDown(KeyCode.A)) {
				GoLeft();
			} else if (Input.GetKeyDown(KeyCode.D)) {
				GoRight();
			}
		}
	}
}
