﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Warehouse {
    public class MainMenuButtons : MonoBehaviour {
		[SerializeField] private MenuSceneDoor m_Door;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Button handler can't be static")]
		public void QuitApplication() {
            Application.Quit();
        }

		public void StartGame() {
			var operation = SceneManager.LoadSceneAsync("DirkScene", LoadSceneMode.Additive);
			operation.completed += LoadScene_Completed;
		}

		private void LoadScene_Completed(AsyncOperation obj) {
			m_Door.Open();
			// TODO close main menu, activate game script in other scene
		}
    }
}
