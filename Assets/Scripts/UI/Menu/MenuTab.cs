﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Warehouse {
	[RequireComponent(typeof(Animator))]
	public class MenuTab : MonoBehaviour {
		[SerializeField, Range(0, 1)] private float m_Opacity = 1;

		private Animator m_Animator;
		private readonly Dictionary<Graphic, float> m_Graphics = new Dictionary<Graphic, float>();
		private float m_PrevOpacity;

		private void Awake() {
			m_Animator = GetComponent<Animator>();
		}

		private void OnEnable() {
			m_PrevOpacity = m_Opacity;
		}

		private void Update() {
			// I'd do this with a property but unity can't serialize those, therefore animations can't use them
			// Though this has the added bonus of not doing it multiple times per frame, if you change it multiple times per frame
			if (m_Opacity != m_PrevOpacity) {
				// Running this in Update is not ideal, but I tried caching the Graphics and updating the cache in OnTransformChildrenChanged, but for some reason that didn't work.
				// If you ever want to try fixing it feel free to checkout commit 769d127.
				// It only runs if the opacity has changed anyway, which in practice means it runs every frame for only about 0.5 seconds every time a tab is closed/opened.
				UpdateChildOpacity();

				m_PrevOpacity = m_Opacity;
			}
		}

		private void UpdateChildOpacity() {
			foreach (Graphic graphic in GetComponentsInChildren<Graphic>(true)) {
				var color = graphic.color;
				if (!m_Graphics.TryGetValue(graphic, out float alpha)) {
					m_Graphics[graphic] = graphic.color.a;
					alpha = graphic.color.a;
				}
				color.a = alpha * m_Opacity;
				graphic.color = color;
			}
		}

		public IEnumerator Open() {
			gameObject.SetActive(true);
			UpdateChildOpacity();
			yield return new WaitForSeconds(TabManager.Instance.InAnimation.length);
		}

		public IEnumerator Close() {
			m_Animator.SetTrigger("Out");
			yield return new WaitForSeconds(TabManager.Instance.OutAnimation.length);
			m_Animator.ResetTrigger("Out");
			gameObject.SetActive(false);
		}
	}
}
