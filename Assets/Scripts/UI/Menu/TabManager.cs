﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Warehouse {
	public class TabManager : Singleton<TabManager> {
		[SerializeField] private MenuTab m_LandingTab;
		[SerializeField] private AnimationClip m_InAnimation;
		[SerializeField] private AnimationClip m_OutAnimation;

		private Stack<MenuTab> m_OpenTabs;

		public event EventHandler<ValueChangedEvent<int>> TabCountChanged;
		
		public AnimationClip InAnimation => m_InAnimation;
		public AnimationClip OutAnimation => m_OutAnimation;

		protected override void Awake() {
			base.Awake();
			m_OpenTabs = new Stack<MenuTab>();
			m_OpenTabs.Push(m_LandingTab);
		}

		public IEnumerator OpenTab(MenuTab tab, MenuTab from) {
			int oldCount = m_OpenTabs.Count;
			MenuTab lastTab = null;
			while (m_OpenTabs.Count > 1 && m_OpenTabs.Peek() != from) {
				lastTab = m_OpenTabs.Pop();

				IEnumerator lastTabClose = lastTab.Close();
				lastTabClose.MoveNext();
				yield return lastTabClose.Current;
				lastTabClose.MoveNext();
			}

			m_OpenTabs.Push(tab);

			IEnumerator tabOpen = tab.Open();
			tabOpen.MoveNext();
			yield return tabOpen.Current;
			tabOpen.MoveNext();

			TabCountChanged?.Invoke(this, new ValueChangedEvent<int>(oldCount, m_OpenTabs.Count));
		}

		public void BackButton() => StartCoroutine(Back());

		private IEnumerator Back() {
			if (m_OpenTabs.Count > 1) {
				var tab = m_OpenTabs.Pop();

				IEnumerator tabClose = tab.Close();
				tabClose.MoveNext();
				yield return tabClose.Current;
				tabClose.MoveNext();

				TabCountChanged?.Invoke(this, new ValueChangedEvent<int>(m_OpenTabs.Count + 1, m_OpenTabs.Count));
			}
		}
	}
}
