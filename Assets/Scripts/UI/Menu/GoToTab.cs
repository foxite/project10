﻿using UnityEngine;
using UnityEngine.UI;

namespace Warehouse {
	// TODO (investigate) Do buttons not work in VR because the clicks are seen as drags and get intercepted by ScrollView?
	// It's theoretically extremely hard to *click* without *dragging* using the VR laser, so this might be worth checking out.
	// I'm not sure what you'd do about it, other than somehow adding a minimum drag distance (not sure how you'd do that either).
	public class GoToTab : MonoBehaviour {
		[SerializeField] private MenuTab m_Tab;

		private void Awake() {
			GetComponent<Button>().onClick.AddListener(() => {
				StartCoroutine(TabManager.Instance.OpenTab(m_Tab, transform.GetComponentInParent<MenuTab>()));
			});
		}
	}
}
