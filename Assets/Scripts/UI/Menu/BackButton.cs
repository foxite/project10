﻿using UnityEngine;
using UnityEngine.UI;

namespace Warehouse {
	public class BackButton : MonoBehaviour {
		private Button m_Button;

		private void Start() {
			TabManager.Instance.TabCountChanged += OnTabCountChanged;
			m_Button = GetComponent<Button>();
			m_Button.onClick.AddListener(TabManager.Instance.BackButton);
		}

		private void OnTabCountChanged(object sender, ValueChangedEvent<int> e) {
			if (e.newValue == 1) {
				m_Button.interactable = false;
			} else {
				m_Button.interactable = true;
			}
		}
	}
}
