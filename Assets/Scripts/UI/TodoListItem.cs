﻿using TMPro;
using UnityEngine;

namespace Warehouse {
	public class TodoListItem : MonoBehaviour {
		[SerializeField] private TMP_Text m_ItemName;
		[SerializeField] private TMP_Text m_Location; // TODO use location

		private Box m_TheBox;

		public Box TheBox {
			get => m_TheBox;
			set {
				m_TheBox = value;
				m_ItemName.text = value.Item.Definition.ItemName;
			}
		}
	}
}