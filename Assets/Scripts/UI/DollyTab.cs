﻿using TMPro;
using UnityEngine;

namespace Warehouse {
	public class DollyTab : MonoBehaviour {
		[SerializeField] private Transform m_StuckText;
		[SerializeField] private TMP_Text m_TimeLeftText;
		[SerializeField] private DollyAgent m_DollyAgent;

		private void Update() {
			if (m_DollyAgent.ValidPath) {
				m_TimeLeftText.text = m_DollyAgent.TimeLeft.ToString("mm':'ss'.'f");
				m_TimeLeftText.gameObject.SetActive(true);
				m_StuckText.gameObject.SetActive(false);
			} else {
				// Doing this in update?
				m_TimeLeftText.gameObject.SetActive(false);
				m_StuckText.gameObject.SetActive(true);
			}
		}
	}
}
