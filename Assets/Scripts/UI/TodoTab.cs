﻿using System.Collections.Generic;
using UnityEngine;

namespace Warehouse {
	public class TodoTab : MonoBehaviour {
		[SerializeField] private TodoListItem m_ListItemPrefab;

		private void Start() {
			TodoList.Instance.BoxAdded += (box) => OnListUpdated();
			TodoList.Instance.BoxRemoved += (box) => OnListUpdated();
		}

		private void OnListUpdated() {
			IReadOnlyList<Box> boxesToGet = TodoList.Instance.BoxesToGet;
			Util.UpdateHierarchyArray(transform, boxesToGet.Count, m_ListItemPrefab.gameObject, (item, i) => {
				item.GetComponent<TodoListItem>().TheBox = boxesToGet[i];
			});
		}
	}
}
