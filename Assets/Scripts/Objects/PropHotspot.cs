﻿using UnityEngine;

namespace Warehouse {
	public class PropHotspot : MonoBehaviour {
		[SerializeField] private PropCategory m_Category;

		public PropCategory Category => m_Category;
	}
}