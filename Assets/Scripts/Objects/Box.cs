﻿using UnityEngine;

namespace Warehouse {
	public class Box : MonoBehaviour {
		public BoxDefinition Definition { get; private set; }
		public Item Item { get; private set; }

		/// <summary>
		/// When instantiating a prefab with this component, call this function to assign an item to it.
		/// </summary>
		/// <param name="itemDef"></param>
		public void AssignDefinitionAndItem(BoxDefinition boxDef, ItemDefinition itemDef) {
			if (Item == null) {
				Definition = boxDef;
				Item = itemDef.CreateInstance(this);
				Item.transform.parent = transform;
			} else {
				Debug.LogError("Tried to assign an item but it was already assigned", this);
			}
		}
	}
}
