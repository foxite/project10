﻿using UnityEngine;

namespace Warehouse {
	public class Item : MonoBehaviour {
		public ItemDefinition Definition { get; set; }
		public Box Box { get; set; }

		/// <summary>
		/// You can't put an item in a box that doesn't fit. If an item is put in a box other than its assigned box, you will lose a small amount of money.
		/// </summary>
		/// <param name="box"></param>
		/// <returns></returns>
		public bool BoxFits(Box box) {
			return box.Definition.Size > Box.Definition.Size;
		}

		public void AssignDefinitionAndBox(ItemDefinition def, Box box) {
			if (Definition == null) {
				Definition = def;
				Box = box;
			} else {
				Debug.LogError("Tried to assign a box but it was already assigned", this);
			}
		}
	}
}
