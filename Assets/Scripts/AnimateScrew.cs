﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Direction { Forward, Backward}
public class AnimateScrew : MonoBehaviour
{
    //Rotational Speed
    public float speed = 100f;

    public Direction Movedirection;


    void Update()
    {
        //Forward Direction
        if (Movedirection == Direction.Forward)
        {
            transform.Rotate(0, 0, -Time.deltaTime * speed, Space.Self);
        }
        //Reverse Direction
        if (Movedirection == Direction.Backward)
        {
            transform.Rotate(0, 0, Time.deltaTime * speed, Space.Self);
        }

    }
}