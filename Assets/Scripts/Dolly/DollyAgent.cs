﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Warehouse {
	/// <summary>
	/// This is a simple wrapper for the Dolly's NavMeshAgent. Don't use the NMA directly -- use this instead.
	/// </summary>
	[RequireComponent(typeof(NavMeshAgent))]
	public class DollyAgent : MonoBehaviour {
		[SerializeField] private Transform m_IndicatorObject;

		private NavMeshAgent m_NMA;

		public bool ValidPath { get; private set; }
		public TimeSpan TimeLeft => TimeSpan.FromSeconds(CalculateDistance() / m_NMA.speed);

		private void Awake() {
			m_NMA = GetComponent<NavMeshAgent>();
		}

		private void Update() {
			if (m_NMA.remainingDistance <= 0.5) {
				m_IndicatorObject.gameObject.SetActive(false);
			}
		}

		public void SetDestination(Vector3 destination) {
			bool hadValidPath = ValidPath;
			ValidPath = m_NMA.SetDestination(destination);
			m_IndicatorObject.gameObject.SetActive(true);
			m_IndicatorObject.transform.position = destination;

			if (!ValidPath) {
				Debug.Log("Dolly can't reach target");
			} else if (hadValidPath && ValidPath) {
				Debug.Log("Dolly now has valid target");
				CalculateDistance();
			}
		}
		
		// Slow as fuck but there's only one instance of this script, and there is no other way (AFAIK)
		// It's possible to only run this once every 10 frames or something but it creates the potential for "jittering" in TimeLeft where it goes below 10 seconds, recalculates,
		//  and then jumps back above 10 seconds.
		private float CalculateDistance() {
			Vector3 prevCorner = transform.position;
			var ret = 0f;
			foreach (Vector3 corner in m_NMA.path.corners) {
				ret += Vector3.Distance(prevCorner, corner);
				prevCorner = corner;
			}
			ret += Vector3.Distance(prevCorner, m_NMA.destination);
			return ret;
		}
	}
}
