﻿using UnityEngine;

namespace Warehouse {
	/// <summary>
	/// This is a way to test the Dolly without needing to put on a full VR set and everything. Just click somewhere and this script will raycast through the mouse, and the Dolly will move to wherever was hit.
	/// </summary>
	[RequireComponent(typeof(Camera))]
	public class MouseDollyController : MonoBehaviour {
		[SerializeField] private DollyAgent m_DollyAgent;

		private Camera m_Camera;

		private void Awake() {
			m_Camera = GetComponent<Camera>();
		}

		private void Update() {
			if (Input.GetMouseButtonDown(0)) {
				Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
				if (Physics.Raycast(ray, out RaycastHit hitInfo)) {
					m_DollyAgent.SetDestination(hitInfo.point);
				}
			}
		}
	}
}
