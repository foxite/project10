﻿using UnityEngine;

namespace Warehouse {
	public class ScannerDollyController : MonoBehaviour {
		[SerializeField] private Transform m_RaycastOrigin;
		[SerializeField] private DollyAgent m_DollyAgent;

		private void Update() {
			if (Input.GetKey(KeyCode.E)) {
				var ray = new Ray(m_RaycastOrigin.position, m_RaycastOrigin.forward);
				if (Physics.Raycast(ray, out RaycastHit hitInfo)) {
					m_DollyAgent.SetDestination(hitInfo.point);
				}
			}
		}
	}
}
