﻿using System.Collections.Generic;
using System;

namespace Warehouse {
	public class TodoList : Singleton<TodoList> {
		private List<Box> m_BoxesToGet;

		public event Action<Box> BoxAdded;
		public event Action<Box> BoxRemoved;

		public IReadOnlyList<Box> BoxesToGet => m_BoxesToGet.AsReadOnly();

		protected override void Awake() {
			base.Awake();
			m_BoxesToGet = new List<Box>();
		}

		public void AddBox(Box box) {
			m_BoxesToGet.Add(box);
			BoxAdded?.Invoke(box);
		}

		public bool RemoveBox(Box box) {
			bool ret = m_BoxesToGet.Remove(box);
			if (ret) {
				BoxRemoved?.Invoke(box);
			}
			return ret;
		}
	}
}
