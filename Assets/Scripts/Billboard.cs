﻿using UnityEngine;

namespace Warehouse {
	/// <summary>
	/// This component will make sure the transform.forward always points to another Transform.
	/// </summary>
	public class Billboard : MonoBehaviour {
		[SerializeField] private Transform m_Target;

		private void Update() {
			transform.forward = transform.position - m_Target.position;
		}
	}
}
