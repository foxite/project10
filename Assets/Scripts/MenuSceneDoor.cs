﻿using UnityEngine;

namespace Warehouse {
	public class MenuSceneDoor : MonoBehaviour {
		private Animator m_Animator;

		private void Awake() {
			m_Animator = GetComponent<Animator>();
		}

		public void Open() {
			m_Animator.SetBool("Open", true);
		}

		public void Close() {
			m_Animator.SetBool("Open", false);
		}
	}
}
