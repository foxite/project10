﻿using UnityEngine;

namespace Warehouse {
	public class ConveyorBelt : MonoBehaviour {
		[SerializeField] private float m_Speed;

		private Rigidbody m_RB;

		private void Awake() {
			m_RB = GetComponent<Rigidbody>();
		}

		private void FixedUpdate() {
			// From https://www.reddit.com/r/Unity3D/comments/7q8jt1/physicsbased_conveyor_belt_almost/
			Vector3 movement = transform.forward * m_Speed * Time.fixedDeltaTime;
			m_RB.position -= movement;
			m_RB.MovePosition(m_RB.position + movement);
		}
	}
}
