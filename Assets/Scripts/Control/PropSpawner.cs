﻿using UnityEngine;

namespace Warehouse {
	public class PropSpawner : MonoBehaviour {
		// Does not actually spawn props yet.
		// It shows you how to spawn a hotspot, though.

		/// <returns>The prop that was spawned</returns>
		private Prop SpawnProp(PropHotspot hotspot) {
			Prop prop = hotspot.Category.GetRandomProp();
			prop.SpawnOnHotspot(hotspot);
			return prop;
		}
	}
}
