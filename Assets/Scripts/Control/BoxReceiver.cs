﻿using UnityEngine;

namespace Warehouse {
	public class BoxReceiver : MonoBehaviour {
		private void OnTriggerEnter(Collider collider) {
			if (collider.TryGetComponent(out Box box)) {
				// TODO check if box was correct and change score accordingly
				Destroy(box.gameObject); // TODO should use object pool instead?
			}
		}
	}
}
