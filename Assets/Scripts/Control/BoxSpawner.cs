﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Warehouse {
	public class BoxSpawner : Singleton<BoxSpawner> {
		[SerializeField] private List<BoxDefinition> m_BoxDefinitions;
		[SerializeField] private List<ItemDefinition> m_ItemDefinitions;

		private List<BoxSpawnPoint> m_SpawnPoints;

		protected override void Awake() {
			base.Awake();
			m_SpawnPoints = new List<BoxSpawnPoint>();
		}

		private Box SpawnBox(BoxSpawnPoint point, BoxDefinition boxDef, ItemDefinition itemDef) {
			Box box = boxDef.CreateInstance(itemDef);
			box.transform.SetPositionAndRotation(point.transform.position, point.transform.rotation);
			point.Occupied = true;
			return box;
		}

		private void Update() {
			// TODO test code
			if (Input.GetKeyDown(KeyCode.P)) {
				Box box = SpawnItem();
				TodoList.Instance.AddBox(box);
			}
		}

		/// <summary>
		/// Spawns an item and a box for it. Returns the box.
		/// </summary>
		public Box SpawnItem() {
			ItemDefinition item = m_ItemDefinitions[Random.Range(0, m_ItemDefinitions.Count)];
			BoxDefinition box = m_BoxDefinitions.Where(def => def.Size >= item.Size).GetRandom();
			if (box == null) {
				Debug.LogError("No boxes can store this item!", item);
				return null;
			}
			IEnumerable<BoxSpawnPoint> points = m_SpawnPoints.Where(thisPoint => !thisPoint.Occupied);
			if (points.Any()) {
				var point = points.GetRandom();
				return SpawnBox(point, box, item);
			} else {
				Debug.LogError("We don't have enough spawn points!", this);
				return null;
			}
		}

		public void RemovePoint(BoxSpawnPoint point) => m_SpawnPoints.Remove(point);
		public void AddPoint(BoxSpawnPoint point) => m_SpawnPoints.Add(point);
	}
}
